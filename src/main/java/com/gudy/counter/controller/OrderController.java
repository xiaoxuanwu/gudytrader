package com.gudy.counter.controller;

import com.gudy.counter.bean.res.*;
import com.gudy.counter.cache.StockCache;
import com.gudy.counter.service.OrderService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;

import static com.gudy.counter.bean.res.CounterRes.*;

/**
 * @PROJECT_NAME: counter
 * @DESCRIPTION:
 * @USER: 涂玄武
 * @DATE: 2020/12/7 13:50
 */
@RestController
@Log4j2
@RequestMapping(value = "/api")
public class OrderController {

    @Autowired
    private StockCache stockCache;

    @RequestMapping(value = "/code")
    public CounterRes stockQuery(@RequestParam String key) {
        Collection<StockInfo> stockInfos = stockCache.getStocks(key);
        return new CounterRes(stockInfos);
    }

    @Autowired
    private OrderService orderService;

    /**
     * 查资金
     *
     * @param uid
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/balance")
    public CounterRes balanceQuery(@RequestParam long uid) throws Exception {
        long balance = orderService.getBalance(uid);
        return new CounterRes(balance);
    }

    /**
     * 查持仓
     *
     * @param uid
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/posiinfo")
    public CounterRes posiQuery(@RequestParam long uid) throws Exception {
        List<PosiInfo> posiList = orderService.getPosiList(uid);
        return new CounterRes(posiList);
    }

    /**
     * 查委托
     *
     * @param uid
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/orderinfo")
    public CounterRes orderQuery(@RequestParam long uid) throws Exception {
        List<OrderInfo> orderList = orderService.getOrderList(uid);
        return new CounterRes(orderList);
    }

    /**
     * 查成交
     *
     * @param uid
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/tradeinfo")
    public CounterRes tradeQuery(@RequestParam long uid) throws Exception {
        List<TradeInfo> tradeList = orderService.getTradeList(uid);
        return new CounterRes(tradeList);
    }

    /**
     * 发送委托
     * @param uid
     * @param type
     * @param timestamp
     * @param code
     * @param direction
     * @param price
     * @param volume
     * @param ordertype
     * @return
     */
    @RequestMapping(value = "/sendorder")
    public CounterRes order(
            @RequestParam int uid,
            @RequestParam short type,
            @RequestParam long timestamp,
            @RequestParam int code,
            @RequestParam byte direction,
            @RequestParam long price,
            @RequestParam long volume,
            @RequestParam byte ordertype
    ) {
        if (orderService.sendOrder(uid, type, timestamp, code, direction, price,
                volume, ordertype)) {
            return new CounterRes(SUCCESS, "save success", null);
        } else {
            return new CounterRes(FAIL, "save failed", null);
        }
    }
}
