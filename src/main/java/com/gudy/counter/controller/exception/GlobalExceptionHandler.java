package com.gudy.counter.controller.exception;

import com.gudy.counter.bean.res.CounterRes;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * @PROJECT_NAME: counter
 * @DESCRIPTION: 全局异常处理类
 * @USER: 涂玄武
 * @DATE: 2020/12/9 15:37
 */

@ControllerAdvice
@ResponseBody
@Log4j2
public class GlobalExceptionHandler {

    @ExceptionHandler(value = Exception.class)
    public CounterRes exceptionHandler(HttpServletRequest request,Exception e){
        log.error(e);
        return new CounterRes(CounterRes.FAIL,"发生错误",null);
    }
}
