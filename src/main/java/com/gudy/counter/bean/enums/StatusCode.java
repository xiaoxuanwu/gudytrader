package com.gudy.counter.bean.enums;

/**
 * @PROJECT_NAME: counter
 * @DESCRIPTION: 通用状态码
 * @USER: 涂玄武
 * @DATE: 2020/11/30 15:00
 */
public enum  StatusCode {

    Success(0,"成功"),
    Fail(-1,"失败"),
    LoginFail(201,"用户名密码/验证码错误，登录失败"),
    ReLogin(202,"请重新登录"),
    ;

    private Integer code;
    private String msg;

    StatusCode(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
