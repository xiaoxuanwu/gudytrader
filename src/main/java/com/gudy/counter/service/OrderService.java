package com.gudy.counter.service;

import com.gudy.counter.bean.res.OrderInfo;
import com.gudy.counter.bean.res.PosiInfo;
import com.gudy.counter.bean.res.TradeInfo;

import java.util.List;

/**
 * @author 0100064695
 */
public interface OrderService {

    /**
     * 查资金
     * @param uid
     * @return
     */
    Long getBalance(long uid);

    /**
     * 查持仓
     * @param uid
     * @return
     */
    List<PosiInfo> getPosiList(long uid);

    /**
     * 查委托
     * @param uid
     * @return
     */
    List<OrderInfo> getOrderList(long uid);

    /**
     * 查成交
     * @param uid
     * @return
     */
    List<TradeInfo> getTradeList(long uid);

    /**
     * 发送委托
     * @param uid
     * @param type
     * @param timestamp
     * @param code
     * @param direction
     * @param price
     * @param volume
     * @param ordertype
     * @return
     */
    boolean sendOrder(long uid,short type,long timestamp,
                      int code,byte direction,long price,long volume,
                      byte ordertype);
}
